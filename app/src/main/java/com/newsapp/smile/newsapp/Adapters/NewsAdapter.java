package com.newsapp.smile.newsapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newsapp.smile.newsapp.Models.News;
import com.newsapp.smile.newsapp.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by smile on 23.11.2016.
 */

public class NewsAdapter extends BaseAdapter {

    private List<News>  newsList;

    private Context context;

    private LayoutInflater layoutInflater;

    public NewsAdapter(Context context,List<News> newsList){
        if (newsList == null){
            this.newsList = new ArrayList<News>();
        }else{
            this.newsList = newsList;
        }
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.newsList.size();
    }

    @Override
    public Object getItem(int i) {
        return this.newsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.newsList.get(i).id;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null){
            view = layoutInflater.inflate(R.layout.news_item, viewGroup, false);
        }

        News news = newsList.get(i);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
        Date result = null;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy ");
        try {
            result = df.parse(news.date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ((TextView) view.findViewById(R.id.tvNameNews)).setText(news.title);
        ((TextView) view.findViewById(R.id.tvDateNews)).setText(sdf.format(result));
        ((TextView) view.findViewById(R.id.tvShortAboutNews)).setText(news.shortDescription);

        return view;
    }

    public void addList(List<News> newsList){
        for (News news: newsList) {
            this.newsList.add(news);
        }
    }
}
