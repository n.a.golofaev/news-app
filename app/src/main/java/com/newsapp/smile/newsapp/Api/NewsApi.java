package com.newsapp.smile.newsapp.Api;

import com.newsapp.smile.newsapp.DataRequest.NewsDatailDataRequest;
import com.newsapp.smile.newsapp.DataRequest.NewsListDataRequest;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by smile on 23.11.2016.
 */

public interface NewsApi {
    @GET("news/categories/{id}/news")
    Call<NewsListDataRequest> getNewsList(@Path("id") Integer id, @Query("page") Integer page);

    @GET("news/details")
    Call<NewsDatailDataRequest> getNewsDetail(@Query("id") Integer id);
}
