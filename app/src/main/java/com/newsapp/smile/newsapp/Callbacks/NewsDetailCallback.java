package com.newsapp.smile.newsapp.Callbacks;

import com.newsapp.smile.newsapp.Models.News;

/**
 * Created by smile on 23.11.2016.
 */

public interface NewsDetailCallback {
    void onSuccess(News news);
}
