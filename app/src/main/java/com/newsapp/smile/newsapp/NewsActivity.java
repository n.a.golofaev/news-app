package com.newsapp.smile.newsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.newsapp.smile.newsapp.Adapters.NewsAdapter;
import com.newsapp.smile.newsapp.Callbacks.NewsListCallback;
import com.newsapp.smile.newsapp.Helpers.ApiHelper;
import com.newsapp.smile.newsapp.Models.News;

import java.io.IOException;
import java.util.List;

public class NewsActivity extends AppCompatActivity {

    private Integer categoryId;

    private ListView listView;

    private NewsAdapter newsAdapter;

    private Integer page = 0;

    private Boolean fullNews = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        Intent intent = getIntent();
        categoryId = Integer.parseInt(intent.getStringExtra("categoryId"));

        listView = (ListView)this.findViewById(R.id.lvNews);

        loadNews(page);

        newsAdapter = new NewsAdapter(getBaseContext(), null);
        listView.setAdapter(newsAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getBaseContext(),NewsDetailActivity.class);
                intent.putExtra("News", (News) adapterView.getAdapter().getItem(i));
                startActivity(intent);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalCount) {
                if (totalCount > 0){
                    Integer lastItem = firstVisibleItem + visibleItemCount;
                    if (lastItem == totalCount){
                        loadNews(page);
                    }
                }
            }
        });
    }

    private void loadNews(Integer pageNumber) {
        ApiHelper.getInstance().getNews(categoryId, pageNumber, new NewsListCallback() {
            @Override
            public void onSuccess(List<News> newsList) {
                if (!fullNews) {
                    if (newsList.size() == 10) {
                        page++;
                    }else{
                        fullNews = true;
                    }
                    newsAdapter.addList(newsList);
                    newsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Integer code) {

            }

            @Override
            public void onError(String errorMessage) {

            }
        });
    }
}
