package com.newsapp.smile.newsapp.Models;

import java.io.Serializable;

/**
 * Created by smile on 23.11.2016.
 */

public class News implements Serializable{
    public Integer id;

    public String date;

    public String fullDescription;

    public String shortDescription;

    public String title;

}
