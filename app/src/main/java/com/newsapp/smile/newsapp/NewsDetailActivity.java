package com.newsapp.smile.newsapp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.newsapp.smile.newsapp.Callbacks.NewsDetailCallback;
import com.newsapp.smile.newsapp.Helpers.ApiHelper;
import com.newsapp.smile.newsapp.Models.News;

public class NewsDetailActivity extends AppCompatActivity {

    private News news;

    private TextView fullDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        news = (News) intent.getSerializableExtra("News");
        this.setTitle(news.title);

        ((TextView) this.findViewById(R.id.tvShortDescriptionNewsDetail)).setText(news.shortDescription);
        fullDescription = (TextView) this.findViewById(R.id.tvFullDescriptionNewsDetail);


        loadFullDescription();
    }

    private void loadFullDescription() {
        ApiHelper.getInstance().getNewsDetail(news.id, new NewsDetailCallback() {
            @Override
            public void onSuccess(News news) {
                if (Build.VERSION.SDK_INT >= 24){
                    fullDescription.setText(Html.fromHtml(news.fullDescription,Html.FROM_HTML_MODE_COMPACT));
                }else {
                    fullDescription.setText(Html.fromHtml(news.fullDescription));
                }
            }
        });
    }
}
