package com.newsapp.smile.newsapp.Callbacks;


import com.newsapp.smile.newsapp.Models.News;

import java.util.List;

/**
 * Created by smile on 23.11.2016.
 */

public interface NewsListCallback {
    void onSuccess(List<News> newsList);

    void onFailure(Integer code);

    void onError(String errorMessage);
}
