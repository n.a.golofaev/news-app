package com.newsapp.smile.newsapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.newsapp.smile.newsapp.Models.Category;
import com.newsapp.smile.newsapp.R;

import java.util.List;

/**
 * Created by smile on 23.11.2016.
 */

public class CategoryAdapter extends BaseAdapter {

    private List<Category> categoryList;

    private Context context;

    private LayoutInflater layoutInflater;

    public CategoryAdapter(Context context,List<Category> categoryList){
        this.categoryList = categoryList;
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int i) {
        return categoryList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return categoryList.get(i).id;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null){
            view = layoutInflater.inflate(R.layout.category_item, viewGroup, false);
        }

        Category category = categoryList.get(i);

        ((TextView) view.findViewById(R.id.tvNameCategory)).setText(category.name);

        return view;
    }
}
