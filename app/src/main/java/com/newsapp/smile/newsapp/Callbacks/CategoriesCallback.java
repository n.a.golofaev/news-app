package com.newsapp.smile.newsapp.Callbacks;

import com.newsapp.smile.newsapp.Models.Category;

import java.util.List;

/**
 * Created by smile on 23.11.2016.
 */

public interface CategoriesCallback {
    void onSuccess(List<Category> categoryList);

    void onFailure(Integer code);

    void onError(String errorMessage);
}
