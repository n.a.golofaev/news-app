package com.newsapp.smile.newsapp;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.newsapp.smile.newsapp.Adapters.CategoryAdapter;
import com.newsapp.smile.newsapp.Callbacks.CategoriesCallback;
import com.newsapp.smile.newsapp.Helpers.ApiHelper;
import com.newsapp.smile.newsapp.Models.Category;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listView;

    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView)this.findViewById(R.id.lvCategories);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getBaseContext(),NewsActivity.class);
                intent.putExtra("categoryId", String.valueOf(adapterView.getAdapter().getItemId(i)));
                startActivity(intent);
            }
        });

        loadCategories();
    }

    private void loadCategories() {
        ApiHelper.getInstance().getCategories(new CategoriesCallback() {
            @Override
            public void onSuccess(List<Category> categoryList) {
                CategoryAdapter categoryAdapter = new CategoryAdapter(getBaseContext(),categoryList);
                listView.setAdapter(categoryAdapter);
            }

            @Override
            public void onFailure(Integer code) {

            }

            @Override
            public void onError(String errorMessage) {
                Snackbar.make(getWindow().getDecorView().getRootView(), errorMessage, Snackbar.LENGTH_LONG)
                        .show();
            }
        });
    }
}
