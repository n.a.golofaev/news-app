package com.newsapp.smile.newsapp.Api;

import com.newsapp.smile.newsapp.DataRequest.CategoriesDataRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Голофаев НА on 23.11.2016.
 */

public interface CategoriesApi {
    @GET("news/categories")
    Call<CategoriesDataRequest> getList();
}
