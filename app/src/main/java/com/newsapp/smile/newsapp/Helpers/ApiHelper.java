package com.newsapp.smile.newsapp.Helpers;


import com.newsapp.smile.newsapp.Api.CategoriesApi;
import com.newsapp.smile.newsapp.Api.NewsApi;
import com.newsapp.smile.newsapp.Callbacks.CategoriesCallback;
import com.newsapp.smile.newsapp.Callbacks.NewsDetailCallback;
import com.newsapp.smile.newsapp.Callbacks.NewsListCallback;
import com.newsapp.smile.newsapp.DataRequest.CategoriesDataRequest;
import com.newsapp.smile.newsapp.DataRequest.NewsDatailDataRequest;
import com.newsapp.smile.newsapp.DataRequest.NewsListDataRequest;
import com.newsapp.smile.newsapp.Models.Category;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Голофаев НА on 23.11.2016.
 */
public class ApiHelper {
    private Retrofit retrofit = null;

    private static ApiHelper ourInstance = new ApiHelper();

    public static ApiHelper getInstance() {
        return ourInstance;
    }

    private ApiHelper() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl("http://testtask.sebbia.com/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public void getCategories(final CategoriesCallback callback) {
        CategoriesApi categoriesApi = retrofit.create(CategoriesApi.class);
        Call<CategoriesDataRequest> call = categoriesApi.getList();

        call.enqueue(new Callback<CategoriesDataRequest>() {
            @Override
            public void onResponse(Call<CategoriesDataRequest> call, Response<CategoriesDataRequest> response) {
                if (response.body().code == 0){
                    callback.onSuccess(response.body().list);
                }else{
                    callback.onFailure(response.body().code);
                }
            }

            @Override
            public void onFailure(Call<CategoriesDataRequest> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    public void getNews(Integer categoryId, Integer page ,final NewsListCallback callback){
        NewsApi newsApi = retrofit.create(NewsApi.class);
        Call<NewsListDataRequest> call = newsApi.getNewsList(categoryId,page);

        call.enqueue(new Callback<NewsListDataRequest>() {
            @Override
            public void onResponse(Call<NewsListDataRequest> call, Response<NewsListDataRequest> response) {
                if (response.body().code == 0){
                    callback.onSuccess(response.body().list);
                }else{
                    callback.onFailure(response.body().code);
                }
            }

            @Override
            public void onFailure(Call<NewsListDataRequest> call, Throwable t) {
                callback.onError(t.getMessage());
            }
        });
    }

    public void getNewsDetail(Integer newsId, final NewsDetailCallback callback) {
        NewsApi newsApi = retrofit.create(NewsApi.class);
        Call<NewsDatailDataRequest> call = newsApi.getNewsDetail(newsId);

        call.enqueue(new Callback<NewsDatailDataRequest>() {
            @Override
            public void onResponse(Call<NewsDatailDataRequest> call, Response<NewsDatailDataRequest> response) {
                if (response.body().code == 0) {
                    callback.onSuccess(response.body().news);
                }
            }

            @Override
            public void onFailure(Call<NewsDatailDataRequest> call, Throwable t) {

            }
        });
    }
}
