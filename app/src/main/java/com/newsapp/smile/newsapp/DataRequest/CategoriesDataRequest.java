package com.newsapp.smile.newsapp.DataRequest;

import com.newsapp.smile.newsapp.Models.Category;

import java.util.List;

/**
 * Created by Голофаев НА on 23.11.2016.
 */

public class CategoriesDataRequest {
    public Integer code;

    public List<Category> list;
}
