package com.newsapp.smile.newsapp.DataRequest;

import com.newsapp.smile.newsapp.Models.News;

import java.util.List;

/**
 * Created by smile on 23.11.2016.
 */

public class NewsListDataRequest {
    public Integer code;

    public List<News> list;
}
